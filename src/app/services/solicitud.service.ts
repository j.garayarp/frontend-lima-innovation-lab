import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { tap } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class SolicitudService {

    constructor(private http: HttpClient) {}

    listar() {
        return this.http.get<any>("http://localhost:8000/solicitud/");
    }

    obtener(id) {
        return this.http.get<any>(`http://localhost:8000/solicitud/${id}/`);
    }

    editar(id, data) {
        return this.http.put<any>(`http://localhost:8000/solicitud/${id}/`, data);
    }


}