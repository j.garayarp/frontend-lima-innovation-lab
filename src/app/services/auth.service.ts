import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { tap } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class AuthService {
    
    constructor(private http: HttpClient) {}

    login(user: {email: string, password: string}) {
        return this.http.post<any>("http://localhost:8000/users/login/", user)
        .pipe(
            tap(response => this.storeData(response))
        )
    }

    logout() {
        localStorage.removeItem('userData');
    }

    getAccessToken() {
        return !!localStorage.getItem('userData');
    }

    storeData(userData) {
        localStorage.setItem('userData', JSON.stringify(userData));
    }

}