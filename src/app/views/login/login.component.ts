import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
})
export class LoginComponent implements OnInit {
  formLogin: FormGroup;
  failLogin: boolean;
  constructor( private fb: FormBuilder, private authService: AuthService, private router: Router) {
    this.failLogin = false;
  }

  ngOnInit() {
    this.initForm();
  }

  initForm() {
    this.formLogin = this.fb.group({
      email: ['', Validators.compose([Validators.required, Validators.email])],
      password: ['', Validators.required]
    })
  }

  login() {
    this.authService.login(this.formLogin.value).subscribe(
      response => {
        if (response) {
          this.router.navigate(['/home']);
          this.failLogin = false;
        } else {
          this.failLogin = true;
        }
      }, error => {
        this.failLogin = true;
      }
    ); 
  }

}
