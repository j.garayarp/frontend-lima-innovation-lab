import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { SolicitudService } from 'src/app/services/solicitud.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
})
export class HomeComponent implements OnInit {
  usuario: string;
  data: any[];
  selectItem: any;

  constructor(
    private authService: AuthService,
    private solicitudService: SolicitudService,
    private router: Router
  ) { 
    this.usuario = JSON.parse(localStorage.getItem('userData')).user['first_name'] + ' ' + JSON.parse(localStorage.getItem('userData')).user['last_name'] || 'anónimo';
    this.data = [];
    this.selectItem = {};
  }

  ngOnInit(): void {
    this.listar();
  }

  listar() {
    this.solicitudService.listar().subscribe(
      response => {
        this.data = response['results']
      }
    )
  }

  revisar () {
    // this.router.navigate([`http://localhost:4200/home/${this.selectItem.id}`]);
    this.router.navigateByUrl(`/home/${this.selectItem.id}`);
  }

  logout() {
    this.authService.logout();
    this.router.navigate(['/login']);
  }
}
