import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { SolicitudService } from 'src/app/services/solicitud.service';

@Component({
    selector: 'app-detalle',
    templateUrl: './detalle.component.html',
})
export class DetalleComponent implements OnInit {
    id: number;
    solicitud: any;

    constructor(
        private solicitudService: SolicitudService,
        private router: Router,
        private activatedRoute: ActivatedRoute
    ) {
        this.activatedRoute.params.subscribe(
            params => {
                this.id = params.id;
            }
            
        )
    }

    ngOnInit() {
        this.obtenerSolicitud();
    }

    obtenerSolicitud() {
        this.solicitudService.obtener(this.id).subscribe(
            response => {
                this.solicitud = response;
            }
        )
    }

    atras() {
        this.router.navigateByUrl(`/home`)
    }
}