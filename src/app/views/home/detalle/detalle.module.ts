import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DetalleComponent } from './detalle.component';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', component: DetalleComponent },
]

@NgModule({
  declarations: [DetalleComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ]
})
export class DetalleModule { }
