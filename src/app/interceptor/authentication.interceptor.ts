import { Injectable } from '@angular/core';
import { HttpHandler, HttpInterceptor, HttpRequest, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {
    token: string;
    constructor() {

    }
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        if (localStorage.getItem('userData')) {
            this.token = JSON.parse(localStorage.getItem('userData')).token;
            request = request.clone({
                setHeaders: {
                    Authorization: 'Bearer ' + this.token
                }
            })
        }
        return next.handle(request);
    }
}