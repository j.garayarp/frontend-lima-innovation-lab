import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './guards/auth.guard';
import { LoginGuard } from './guards/login.guard';

const routes: Routes = [
  { 
    path: '',
    canActivate: [AuthGuard],
    redirectTo: '/home', pathMatch: 'full'
  },
  { 
    path: 'login',
    canActivate: [LoginGuard],
    loadChildren: () => import('./views/login/login.module').then(m => m.LoginModule) 
  },
  { 
    path: 'home', 
    canActivate: [AuthGuard],
    loadChildren: () => import('./views/home/home.module').then(m => m.HomeModule) 
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
