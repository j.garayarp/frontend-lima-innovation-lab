FROM node:14.9.0 as node
WORKDIR /app
COPY ./ /app/
RUN npm install
ARG configuration=production
RUN npm run build -- --prod --configuration=$configuration

FROM nginx:1.19.2-alpine
COPY --from=node /app/dist/NewSchool /usr/share/nginx/html
COPY ./nginx-custom.conf /etc/nginx/conf.d/default.conf